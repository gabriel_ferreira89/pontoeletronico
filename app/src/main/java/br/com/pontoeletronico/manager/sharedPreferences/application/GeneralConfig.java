package br.com.pontoeletronico.manager.sharedPreferences.application;

import android.content.Context;
import android.content.SharedPreferences;

import br.com.pontoeletronico.manager.sharedPreferences.SharedPreferencesUtil;


public class GeneralConfig {
	
	private SharedPreferences generalConfigPreferences;

	public static final String SHARED_PREFERENCES_FILE_NAME	= "PontoEletronicoApp.conf";

	public GeneralConfig(Context context) {
		this.generalConfigPreferences = SharedPreferencesUtil.getGeneralConfigPreferences();
	}

}
