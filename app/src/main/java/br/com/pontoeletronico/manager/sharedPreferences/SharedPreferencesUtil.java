package br.com.pontoeletronico.manager.sharedPreferences;

import android.content.Context;
import android.content.SharedPreferences;

import br.com.pontoeletronico.PontoEletronicoApplication;
import br.com.pontoeletronico.manager.sharedPreferences.application.GeneralConfig;
import br.com.pontoeletronico.manager.sharedPreferences.session.SessionData;


public class SharedPreferencesUtil {

    private SharedPreferencesUtil() { }

    public static SharedPreferences getGeneralConfigPreferences() {
        return PontoEletronicoApplication.getInstance().getSharedPreferences(GeneralConfig.SHARED_PREFERENCES_FILE_NAME, Context.MODE_PRIVATE);
    }

    public static SharedPreferences getSessionData() {
        return PontoEletronicoApplication.getInstance().getSharedPreferences(SessionData.SHARED_PREFERENCES_FILE_NAME, Context.MODE_PRIVATE);
    }
}
