package br.com.pontoeletronico.manager;

import br.com.pontoeletronico.manager.sharedPreferences.session.SessionData;
import br.com.pontoeletronico.model.User;

public class APIManager {

    private static APIManager instance;
    private static SessionData sessionData;
    private static User user;


    public static APIManager getInstance() {
        if (instance == null) {
            instance = new APIManager();
            sessionData = new SessionData();
            user = sessionData.getUser();
        }
        return instance;
    }

    public User getUser(){
        user = sessionData.getUser();
        return user;
    }
}
