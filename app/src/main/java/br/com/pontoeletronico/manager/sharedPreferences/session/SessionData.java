package br.com.pontoeletronico.manager.sharedPreferences.session;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.io.Serializable;

import br.com.pontoeletronico.manager.sharedPreferences.SharedPreferencesUtil;
import br.com.pontoeletronico.model.User;
import br.com.pontoeletronico.util.ObjectSerializer;


public class SessionData {

	public static final String SHARED_PREFERENCES_FILE_NAME	= "PontoEletronicoAppSession.conf";


	private static Gson gson;

    static {
        gson = new GsonBuilder().create();
    }

	private SharedPreferences sessionData;

	public SessionData() {
		this.sessionData = SharedPreferencesUtil.getSessionData();
	}

	protected static final String USER = "USER";
	protected static final String USER_DEFAULT = "";


	public User getUser() {

		User user = null;
		try {
			user = (User) ObjectSerializer.deserialize(sessionData.getString(USER, USER_DEFAULT));
		} catch (IOException e) {
			e.printStackTrace();
		}
		if (user == null){
			user = new User();
		}
		return user;
	}

	@SuppressLint("CommitPrefEdits")
	public void setUser(User user) {

		if (user == null) {
			user = new User();
		}
		String serializedList = "";
		try {
			serializedList = ObjectSerializer.serialize((Serializable) user);
		} catch (IOException e) {
			e.printStackTrace();
		}
		sessionData.edit().putString(USER, serializedList).commit();
	}

	@SuppressLint("CommitPrefEdits")
	public void clearCache(){
		sessionData.edit().clear().commit();
	}
}
