package br.com.pontoeletronico.util;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;

import java.util.ArrayList;

/**
 * Created by Gabriel F on 04/01/2016.
 */
public class PermissionUtils {


    private static PermissionUtils permissionManager;

    private Activity mActivity;

    public Activity getActivity() {
        return mActivity;
    }

    public void setActivity(Activity mActivity) {
        this.mActivity = mActivity;
    }

    public static PermissionUtils getInstance(){

        if (permissionManager == null){
            permissionManager = new PermissionUtils();
        }

        return permissionManager;
    }

    private PermissionUtils(){

    }

    public void grantPermissions(String[] permission){
        grantPermissions(permission, false, null);
    }
    public void grantPermissions(String permission){
        grantPermissions(new String[]{permission}, false, null);
    }

    public void grantPermissions(final String[] permission, boolean showRequestPermissionRationale, final DialogInterface.OnClickListener cancelPermissionRationale){

        grantPermissions(permission, 0);
    }

    public void grantPermissions(final String[] permission, final int requestCode){

        if (isMashmallowOrGreater() && mActivity != null) { // Marshmallow+
            final ArrayList<String> pendingPerms = new ArrayList();
            for (String perm : permission) {
                int hasPermission = ContextCompat.checkSelfPermission(mActivity, perm);
                if (hasPermission != PackageManager.PERMISSION_GRANTED) {
                    pendingPerms.add(perm);
                }
            }
            if (pendingPerms.size() > 0){
//                for (String perm : pendingPerms) {
//                    if (!ActivityCompat.shouldShowRequestPermissionRationale(mActivity, perm) && showRequestPermissionRationale) {
//                        showMessageOKCancel("É necessario autorização para continuar...",
//                                new DialogInterface.OnClickListener() {
//                                    @Override
//                                    public void onClick(DialogInterface dialog, int which) {
//                                        ActivityCompat.requestPermissions(mActivity, pendingPerms.toArray(new String[pendingPerms.size()]), requestCode);
//                                    }
//                                }, cancelPermissionRationale);
//                        return;
//                    }
////                    ActivityCompat.requestPermissions(mActivity, pendingPerms.toArray(new String[pendingPerms.size()], 0);
//                }
                ActivityCompat.requestPermissions(mActivity, pendingPerms.toArray(new String[pendingPerms.size()]), requestCode);
            }
        }
    }

    public static boolean isMashmallowOrGreater(){

        return Build.VERSION.SDK_INT >= 23;
    }

    /**
     *
     * @param permission - do tipo Manifest.permision
     * @return
     */
    public boolean isPermissionGranted(final String permission){
        if (isMashmallowOrGreater()) { // Marshmallow+
            if (mActivity != null){
                int hasPermission = ActivityCompat.checkSelfPermission(mActivity, permission);
                return (hasPermission == PackageManager.PERMISSION_GRANTED);
            }else{
                return false;
            }
        }else{
            return true;
        }
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener, DialogInterface.OnClickListener cancelListener ) {
        new AlertDialog.Builder(mActivity)
                .setMessage(message)
                .setPositiveButton("AUTORIZAR", okListener)
                .setNegativeButton("Cancel", cancelListener)
                .create()
                .show();
    }
}