package br.com.pontoeletronico.util;


public class Constants {

    public static final String SHARED_PREFERENCES_SESSION_FILE_NAME = "SHARED_PREFERENCES_SESSION_FILE_NAME";
    public static final String SHARED_PREFERENCES_CONFIG_FILE_NAME = "SHARED_PREFERENCES_CONFIG_FILE_NAME";
    public static final String FIREBASE_URL = "https://trabalhei-6f456.firebaseio.com/";

    public static final String KEY_SIGNUP_EMAIL = "SIGNUP_EMAIL";
    public static final String FIREBASE_PROPERTY_TIMESTAMP = "timestamp";

    public static final String FIREBASE_LOCATION_USERS = "users";

    public static final String FIREBASE_URL_USERS = FIREBASE_URL + "/" + FIREBASE_LOCATION_USERS;
    public static final int RC_SIGN_IN = 666;
    public static final String FIREBASE_URL_ACTIVE_USERS = FIREBASE_URL + "USER/";

    public static final String DB_TAG_RECORDS = "records";
    public static final String DB_TAG_RECORDS_LIST = "recordsList";
    public static final String DB_TAG_LAST_TIME_UPDATE = "lastTimeUpdated";
    public static String DATETIME_FORMATER_YYYYMMdd = "YYYYMMdd";
    public static final long SPLASH_TIME_OUT = 2000;
}
