package br.com.pontoeletronico.model;

import com.google.firebase.database.DataSnapshot;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Gabriel on 22/07/2016.
 */
public class RecordList {

    private List<DayRecord> daysList;
    private String lastTimeUpdated;


    public RecordList() {
        this.daysList = new ArrayList<>();
        this.lastTimeUpdated = new DateTime().toString();
    }

    public RecordList(DataSnapshot dataSnapshot) {
        this.daysList = new ArrayList<>();
        this.lastTimeUpdated = dataSnapshot.child("lastTimeUpdated").getKey();
    }

    public List<DayRecord> getDaysList() {
        return daysList;
    }

    public void setDaysList(List<DayRecord> daysList) {
        this.daysList = daysList;
    }

    public String getLastTimeUpdated() {
        return lastTimeUpdated;
    }

    public void setLastTimeUpdated(String lastTimeUpdated) {
        this.lastTimeUpdated = lastTimeUpdated;
    }
}
