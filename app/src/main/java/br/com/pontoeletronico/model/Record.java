package br.com.pontoeletronico.model;

import android.support.annotation.NonNull;

import com.google.gson.annotations.Expose;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Instant;
import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;

import java.io.Serializable;
import java.util.HashMap;

import br.com.pontoeletronico.util.Constants;


public class Record implements Serializable, Comparable<Record> {
    @Expose
    private String date;
    @Expose
    private String obs;
    private String id;

    public Record(String date) {
        this.date = date;
        this.obs = "";
    }
    public Record(String date, String obs) {
        this.date = date;
        this.obs = obs;
    }

    public Record(String recordId, String dateTime, String obs) {
        this.id = recordId;
        this.date = dateTime;
        this.obs = obs;
    }

    public String getDate() {
        DateTime dateTime = new LocalDateTime(date).toDateTime();
        return DateTimeFormat.forPattern(Constants.DATETIME_FORMATER_YYYYMMdd).print(dateTime);
    }

    public DateTime getDateTime() {
        return new Instant(date).toDateTime().withZone(DateTimeZone.UTC);
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getObs() {
        return obs;
    }

    public void setObs(String obs) {
        this.obs = obs;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public int compareTo(@NonNull Record another) {
        return new DateTime(date).compareTo(another.getDateTime());
    }

    public HashMap toMap(String id){

        HashMap<String, Object> map = new HashMap<>();
        map.put("date", date);
        map.put("obs", obs);
        map.put("id", id);

        return map;
    }
}
