package br.com.pontoeletronico.model;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import br.com.pontoeletronico.util.Constants;

/**
 * Created by Gabriel on 27/07/2016.
 */
public class DayRecord {

    private String day;
    private List<Record> recordList;

    public DayRecord(String day) {
        this.day = day;
        this.recordList = new ArrayList<>();
    }

    public DayRecord(List<Record> recordList) {
        this.recordList = recordList;
    }

    public DayRecord() {
        this.recordList = new ArrayList<>();
    }

    public List<Record> getRecordList() {
        Collections.sort(recordList, new Comparator<Record>(){
            public int compare(Record emp1, Record emp2) {
                return emp1.getDateTime().compareTo(emp2.getDateTime());
            }
        });
        return recordList;
    }

    public void setRecordList(List<Record> recordList) {
        this.recordList = recordList;
    }

    public String getDay() {
        return day;
    }


    public DateTime getDayDateTime() {
        return DateTimeFormat.forPattern(Constants.DATETIME_FORMATER_YYYYMMdd).parseDateTime(day);
    }



    public void setDay(String day) {
        this.day = day;
    }
}
