package br.com.pontoeletronico;

import android.app.Application;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.firebase.client.Firebase;
import com.google.firebase.database.FirebaseDatabase;

import br.com.pontoeletronico.util.Constants;
import io.fabric.sdk.android.Fabric;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;


public class PontoEletronicoApplication extends Application {

    private static PontoEletronicoApplication instance;

    public static PontoEletronicoApplication getInstance() {
        if (instance == null){
            instance = new PontoEletronicoApplication();
        }
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        if (BuildConfig.enableCrashlytics){
            Fabric.with(this, new Answers(), new Crashlytics());
        }
        instance = this;
        configureCalligraphy();
        Firebase.setAndroidContext(this);
        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
    }

    private void configureCalligraphy() {
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setFontAttrId(R.attr.fontPath)
                .build());
    }

    public static Firebase getFirebaseRef() {
        return new Firebase(Constants.FIREBASE_URL);
    }

}
