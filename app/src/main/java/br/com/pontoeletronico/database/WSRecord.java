package br.com.pontoeletronico.database;

import com.firebase.client.Firebase;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import org.joda.time.DateTime;

import java.util.HashMap;
import java.util.Map;

import br.com.pontoeletronico.PontoEletronicoApplication;
import br.com.pontoeletronico.manager.sharedPreferences.session.SessionData;
import br.com.pontoeletronico.model.Record;
import br.com.pontoeletronico.model.User;
import br.com.pontoeletronico.util.Constants;

/**
 * Created by GabrielFerreira on 25/08/2016.
 */
public class WSRecord {

    private static WSRecord instance;
    private static Firebase firebaseRef;
    private static DatabaseReference mDatabase;
    private String userId;

    public String getUserId() {
        return new SessionData().getUser().getUid();
    }

    public void setUserId(String userId) {
       this.userId = userId;
    }


    public static WSRecord getInstance() {
        if (instance == null){
            instance = new WSRecord();
            firebaseRef = PontoEletronicoApplication.getFirebaseRef();
            mDatabase = FirebaseDatabase.getInstance().getReference();
        }
        return instance;
    }

    public void writeNewRecord(Record record) {
        String key = mDatabase.child(Constants.DB_TAG_RECORDS)
                .child(getUserId())
                .child(record.getDate())
                .push().getKey();
        Gson gson = new Gson();
        record.setId(key);
        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put("/" + Constants.DB_TAG_RECORDS + "/" + getUserId() + "/"
                + Constants.DB_TAG_RECORDS_LIST + "/"
                + record.getDate()
                + "/" + key, gson.toJson(record));
        childUpdates.put("/" + Constants.DB_TAG_RECORDS + "/" + getUserId() + "/"
                + Constants.DB_TAG_LAST_TIME_UPDATE, new DateTime().toString());

        mDatabase.updateChildren(childUpdates);
    }

    public void retrieveDayRecord(ValueEventListener valueEventListener, String day) {
        mDatabase.child(Constants.DB_TAG_RECORDS).child(getUserId()).child(Constants.DB_TAG_RECORDS_LIST).child(day).addValueEventListener(valueEventListener);
    }

    public void deleteDayRecord(DatabaseReference.CompletionListener completionListener, String day, String recordId){
        mDatabase.child(Constants.DB_TAG_RECORDS).child(getUserId()).child(Constants.DB_TAG_RECORDS_LIST).child(day).child(recordId).removeValue(completionListener);
    }

    public void writeUser(OnSuccessListener<Void> onSuccessListener, final User user){
        mDatabase.child("users").child(user.getUid()).setValue(user).addOnSuccessListener(onSuccessListener);
    }
}
