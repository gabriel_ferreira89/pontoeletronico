package br.com.pontoeletronico.ui.home.monthTab;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.joda.time.DateTime;
import org.joda.time.Instant;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import br.com.pontoeletronico.R;
import br.com.pontoeletronico.model.DayRecord;
import br.com.pontoeletronico.model.Record;
import br.com.pontoeletronico.model.RecordList;
import br.com.pontoeletronico.ui.StandardActivity;
import br.com.pontoeletronico.ui.home.StandardFragment;
import br.com.pontoeletronico.util.AnimUtils;
import br.com.pontoeletronico.util.Constants;
import br.com.pontoeletronico.util.Utils;
import butterknife.Bind;
import butterknife.ButterKnife;

public class MonthTabFragment extends StandardFragment {


    @Bind(R.id.linearlayout_month_recordlist)
    LinearLayout dataLinearLayout;
    @Bind(R.id.refreshlayout_month_refresh)
    SwipeRefreshLayout swipeRefreshLayout;

    private LayoutInflater inflater;
    private View view;
    private List<LinearLayout> expandableViews = new ArrayList<>();
    private DatabaseReference mDatabase;
    String TAG = "MonthTabFragment";
    private RecordList recordList;
    private String userId;

    public static MonthTabFragment newInstance() {
        return new MonthTabFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (view != null){
            return view;
        }
        view = inflater.inflate(R.layout.fragment_month_tab, container, false);
        ButterKnife.bind(this, view);
        this.inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        initListeners();
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mDatabase = FirebaseDatabase.getInstance().getReference();
        userId = ((StandardActivity)getActivity()).getUid();
        retrieveRegisterList();
    }

    private void retrieveRegisterList() {
        mDatabase.child(Constants.DB_TAG_RECORDS).child(userId).addValueEventListener(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        recordList = new RecordList();
                        try{
                            DataSnapshot friendsSnapshot = dataSnapshot.child(Constants.DB_TAG_RECORDS_LIST);
                            for (DataSnapshot friendSnapshot : friendsSnapshot.getChildren()) {
                                DayRecord dayRecord = new DayRecord(friendSnapshot.getKey());
                                for (DataSnapshot innerSnapshot : friendSnapshot.getChildren()) {
                                    Record record = (Record) Utils.parseJsonToObject(new JSONObject(innerSnapshot.getValue().toString()), Record.class);
                                    dayRecord.getRecordList().add(record);
                                }
                                recordList.getDaysList().add(dayRecord);
                                recordList.setLastTimeUpdated(dataSnapshot.child(Constants.DB_TAG_LAST_TIME_UPDATE).getValue().toString());
                            }
                            setupCells(recordList);
                        }catch (Exception ignored){

                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.w(TAG, "getUser:onCancelled", databaseError.toException());
                    }
                });
    }

    private void initListeners() {
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                retrieveRegisterList();
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    private void setupCells(RecordList recordList) {

        dataLinearLayout.removeAllViews();
        List<DayRecord> distinctRecordDates = recordList.getDaysList();
        DateTime lastDate = null;

        Collections.sort(distinctRecordDates, new Comparator<DayRecord>(){
            public int compare(DayRecord emp1, DayRecord emp2) {
                return emp2.getDayDateTime().compareTo(emp1.getDayDateTime());
            }
        });

        for (int i = 0; i< distinctRecordDates.size(); i++) {
            DayRecord dayRecord = distinctRecordDates.get(i);
            DateTime dateTime = dayRecord.getDayDateTime();
            if (lastDate == null || lastDate.getMonthOfYear() != dateTime.getMonthOfYear()){
                View myView = createMonthView(dateTime);
                dataLinearLayout.addView(myView);
            }
            View myView = createDayView(dayRecord.getRecordList(), dateTime);
            dataLinearLayout.addView(myView);
            lastDate = dateTime;
        }
    }

    @NonNull
    private View createMonthView(DateTime dateTime) {
        View myView = inflater.inflate(R.layout.item_month_list, dataLinearLayout, false);
        ((TextView)myView.findViewById(R.id.textview_item_month_date)).setText(dateTime.monthOfYear().getAsText());
        myView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(), "CLick", Toast.LENGTH_SHORT).show();
            }
        });
        return myView;
    }

    @NonNull
    private View createDayView(List<Record> list, DateTime dateTime) {
        final View myView = inflater.inflate(R.layout.item_month_list, dataLinearLayout, false);
        final LinearLayout expandableView = (LinearLayout) myView.findViewById(R.id.linearlayout_item_month_expandable_view);
        String dateformat = getResources().getString(R.string.schedule_time_no_space);
        DateTimeFormatter dtf = DateTimeFormat.forPattern("dd");
        dateformat = dateformat.replace("%d", dtf.print(dateTime));
        dateformat = dateformat.replace("%m", dateTime.monthOfYear().getAsText().substring(0,3));

        Collections.sort(list, new Comparator<Record>(){
            public int compare(Record emp1, Record emp2) {
                return emp1.getDateTime().compareTo(emp2.getDateTime());
            }
        });

        ((TextView)myView.findViewById(R.id.textview_item_month_date)).setText(dateformat);
        setTotalHours(list, (TextView)myView.findViewById(R.id.textview_item_month_time));

        for (int j=0; j< list.size(); j++ ) {
            Record record = list.get(j);
            TextView textView = new TextView(getActivity());
            if (j%2 == 0){
                textView.setText("Entrada - ".concat(DateTimeFormat.forPattern("HH:mm").print(record.getDateTime())));
            }else{
                textView.setText("Saida - ".concat(DateTimeFormat.forPattern("HH:mm").print(record.getDateTime())));
            }
            expandableView.addView(textView);
        }

        myView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (myView.findViewById(R.id.linearlayout_item_month_expandable_view).getVisibility() == View.VISIBLE){
                    AnimUtils.collapse(expandableView);
                }else{
                    AnimUtils.expand(expandableView);
                }
            }
        });
        myView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Toast.makeText(getActivity(), "OnLongClick", Toast.LENGTH_SHORT).show();
                return false;
            }
        });
        expandableViews.add((LinearLayout) myView.findViewById(R.id.linearlayout_item_month_expandable_view));
        return myView;
    }

    public void setTotalHours(List<Record> recordsOfTheDay, TextView textView) {

        long time = 0;

        for (int i = 0; i < recordsOfTheDay.size(); i=i+2) {
            if (recordsOfTheDay.size()> i+1){
                time += recordsOfTheDay.get(i+1).getDateTime().getMillis()-recordsOfTheDay.get(i).getDateTime().getMillis();
            }
        }
        Instant totalHours = new Instant(time);
        DateTimeFormatter otherFormat = DateTimeFormat.forPattern("HH:mm");

        if (totalHours.getMillis() == 0){
            textView.setText("--:--");
        }else if (totalHours.isBefore(8 * 60 * 60 * 1000)){
            textView.setTextColor(getResources().getColor(R.color.month_red_d3));
            textView.setText("-".concat(otherFormat.print(new Instant(8 * 60 * 60 * 1000 - totalHours.getMillis()))));
        }else{
            textView.setTextColor(getResources().getColor(R.color.green));
            textView.setText("+".concat(otherFormat.print(totalHours.minus(8 * 60 * 60 * 1000))));
        }
    }

    @Override
    public void onFabClick() {
//        FlurryAgent.logEvent("fab click - month");
//        RecordEditDialog dialog = new RecordEditDialog(getActivity(), new Record(new DateTime()));
//        dialog.setCancelable(true);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.show();
    }

    public void closeExpandableViews(){
        for (LinearLayout view: expandableViews) {
            AnimUtils.collapse(view);
        }
    }
}
