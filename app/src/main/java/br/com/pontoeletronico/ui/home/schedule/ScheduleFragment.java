package br.com.pontoeletronico.ui.home.schedule;

import android.app.TimePickerDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import org.joda.time.DateTime;
import org.joda.time.Instant;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONObject;

import java.util.Collections;
import java.util.List;

import br.com.pontoeletronico.R;
import br.com.pontoeletronico.database.WSRecord;
import br.com.pontoeletronico.model.DayRecord;
import br.com.pontoeletronico.model.Record;
import br.com.pontoeletronico.ui.home.StandardFragment;
import br.com.pontoeletronico.util.Constants;
import br.com.pontoeletronico.util.Utils;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ScheduleFragment extends StandardFragment implements ScheduleListener{
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    @Bind(R.id.listview_main_recordlist)
    ListView recordListView;
    @Bind(R.id.textview_main_day)
    TextView dayTextView;
    @Bind(R.id.textview_main_total)
    TextView totalTextView;
    @Bind(R.id.linearlayout_record_status)
    LinearLayout statusLinearLayout;
    @Bind(R.id.linearlayout_main_message)
    LinearLayout messageLinearLayout;
    @Bind(R.id.textview_main_message)
    TextView messageTextView;
    @Bind(R.id.fab)
    FloatingActionButton fab;

    private DateTime searchDate = new DateTime();
    private View view;
    private ScheduleAdapter homeAdapter;
    private ScheduleListener listener;
    private DayRecord dayRecord;
    private DayRecord records;
    private static IntentFilter intentFilter;

    private final BroadcastReceiver timeChangedReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if (action.equals(Intent.ACTION_TIME_CHANGED) ||
                    action.equals(Intent.ACTION_TIMEZONE_CHANGED) || action.equals(Intent.ACTION_TIME_TICK)) {
                updateActualSituation(records);
            }
        }
    };

    static {
        intentFilter = new IntentFilter();
        intentFilter.addAction(Intent.ACTION_TIME_TICK);
        intentFilter.addAction(Intent.ACTION_TIMEZONE_CHANGED);
        intentFilter.addAction(Intent.ACTION_TIME_CHANGED);
    }

    public static ScheduleFragment newInstance() {
        return new ScheduleFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (view != null){
            return view;
        }
        view = inflater.inflate(R.layout.fragment_main_tab, container, false);
        try {
            ButterKnife.bind(this, view);
        } catch (Exception e) {
            e.printStackTrace();
        }
        listener = this;
        initListeners();
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        dayRecord = new DayRecord();
        dayRecord.setDay(DateTimeFormat.forPattern(Constants.DATETIME_FORMATER_YYYYMMdd).print(new DateTime()));
        retrieveDayRecord();
    }

    private void retrieveDayRecord() {
        WSRecord.getInstance().retrieveDayRecord(new ValueEventListener() {
            @Override
            public void onDataChange(final DataSnapshot dataSnapshot) {
                dayRecord = new DayRecord(DateTimeFormat.forPattern(Constants.DATETIME_FORMATER_YYYYMMdd).print(new DateTime()));
                try{
                    for (DataSnapshot innerSnapshot : dataSnapshot.getChildren()) {
                        Record record = (Record) Utils.parseJsonToObject(new JSONObject(innerSnapshot.getValue().toString()), Record.class);
                        dayRecord.getRecordList().add(record);
                    }
                }catch (Exception ignored) {

                }
                Collections.sort(dayRecord.getRecordList());
                setupAdapter(dayRecord);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        }, dayRecord.getDay());
    }

    @Override
    public void onResume() {
        super.onResume();
        searchDate = new DateTime();
        setupAdapter(dayRecord);
        initListeners();
        if (new DateTime().getDayOfWeek() == 7){
            recordListView.setVisibility(View.GONE);
            messageLinearLayout.setVisibility(View.VISIBLE);
        }else{
            recordListView.setVisibility(View.VISIBLE);
            messageLinearLayout.setVisibility(View.GONE);
        }
        getActivity().registerReceiver(timeChangedReceiver, intentFilter);
    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(timeChangedReceiver);
    }

    private void updateActualSituation(DayRecord dayRecord) {
        if (dayRecord == null){
            return;
        }
        LocalDate date = new LocalDate();
        String dateformat = getResources().getString(R.string.schedule_time);
        DateTimeFormatter dtf = DateTimeFormat.forPattern("dd");
        dateformat = dateformat.replace("%d", dtf.print(date));
        dateformat = dateformat.replace("%m", date.monthOfYear().getAsText());
        dayTextView.setText(dateformat);

        Instant totalHours = totalHoursToday(dayRecord);
        DateTimeFormatter otherFormat = DateTimeFormat.forPattern("HH:mm");

        if (totalHours.getMillis() == 0){
            statusLinearLayout.setBackgroundResource(R.color.main_time_grey);
            totalTextView.setText("--:--");
        }else if (totalHours.isBefore(8 * 60 * 60 * 1000)){
            totalTextView.setText("-".concat(otherFormat.print(new Instant(8 * 60 * 60 * 1000 - totalHours.getMillis()))));
            statusLinearLayout.setBackgroundResource(R.color.main_time_red);
//            totalTextView.setTextColor(getResources().getColor(R.color.red));
        }else{
            totalTextView.setText("+".concat(otherFormat.print(totalHours.minus(8 * 60 * 60 * 1000))));
            statusLinearLayout.setBackgroundResource(R.color.main_time_green);
//            totalTextView.setTextColor(getResources().getColor(R.color.green));
        }
    }

    private void initListeners() {
        recordListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
                new AlertDialog.Builder(getActivity())
                        .setMessage("Deseja deletar este registro?")
                        .setCancelable(false)
                        .setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                onRecordDelete(homeAdapter.getItem(position));
                            }
                        })
                        .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        }).show();
                return true;
            }
        });
    }

    private void setupAdapter(DayRecord dayRecord) {

        homeAdapter = new ScheduleAdapter(getActivity(), dayRecord.getRecordList(), listener);
        recordListView.setAdapter(homeAdapter);
        records = dayRecord;
        updateActualSituation(dayRecord);
    }

    public Instant totalHoursToday(DayRecord dayRecord) {

        LocalDateTime now = new LocalDateTime();
        long time = 0;
        List<Record> recordsOfTheDay = dayRecord.getRecordList();

        for (int i = 0; i < recordsOfTheDay.size(); i=i+2) {
            if (recordsOfTheDay.size()> i+1){
                time += recordsOfTheDay.get(i+1).getDateTime().getMillisOfDay()-recordsOfTheDay.get(i).getDateTime().getMillisOfDay();
            }else{
                time += now.getMillisOfDay()-recordsOfTheDay.get(i).getDateTime().getMillisOfDay();
            }
        }
        return new Instant(time);
    }

    @Override
    public void onRecordDelete(Record record) {
        dayRecord = new DayRecord();
        dayRecord.setDay(DateTimeFormat.forPattern(Constants.DATETIME_FORMATER_YYYYMMdd).print(new DateTime()));

        WSRecord.getInstance().deleteDayRecord(new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                Toast.makeText(getActivity(), "Registro deletado", Toast.LENGTH_SHORT).show();
            }
        }, record.getDate(), record.getId());

    }

    @Override
    public void onFabClick() {
        new TimePickerDialog(getContext(), new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                if (view.isShown()) {
                    DateTime dateTime = new DateTime(searchDate.getYear(), searchDate.getMonthOfYear(), searchDate.getDayOfMonth(),
                            hourOfDay, minute);
                    WSRecord.getInstance().writeNewRecord(new Record(dateTime.toLocalDateTime().toString()));
                }
            }
        },
                DateTime.now().getHourOfDay(),
                DateTime.now().getMinuteOfHour(), true).show();
    }

    @OnClick(R.id.linearlayout_record_status)
    void onStatusClick(){

        DateTimeFormatter otherFormat = DateTimeFormat.forPattern("HH:mm");
        if (records != null && records.getRecordList().size() >0){
            long time = 0;
            LocalDateTime now = new LocalDateTime();
            for (int i = 0; i < records.getRecordList().size(); i=i+2) {
                if (records.getRecordList().size()> i+1){
                    time += records.getRecordList().get(i+1).getDateTime().getMillisOfDay()-records.getRecordList().get(i).getDateTime().getMillisOfDay();
                }else{
                    time += now.getMillisOfDay()-records.getRecordList().get(i).getDateTime().getMillisOfDay();
                }
            }
            Instant totalHours = new Instant(time);
            long exitHour;
            if (time > 8*60*60*1000){
                exitHour = 0;
            }else{
                exitHour = 8*60*60*1000 - time;
            }
            if (records.getRecordList().size() % 2 == 0){
                showOkAlertDialog("Você já fez ".concat(otherFormat.print(totalHours)));
            }else{
                if (8*60*60*1000 >totalHours.getMillis()){
                    showOkAlertDialog("Você poderá sair às ".concat(otherFormat.print(new DateTime().plus(exitHour))));
                }else{
                    showOkAlertDialog("Você já fez ".concat(otherFormat.print(totalHours.minus(8*60*60*1000))));
                }
            }
        }
    }
}