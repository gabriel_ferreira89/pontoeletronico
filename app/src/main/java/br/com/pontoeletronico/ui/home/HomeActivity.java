package br.com.pontoeletronico.ui.home;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import org.joda.time.DateTime;

import br.com.pontoeletronico.R;
import br.com.pontoeletronico.manager.sharedPreferences.session.SessionData;
import br.com.pontoeletronico.ui.StandardActivity;
import br.com.pontoeletronico.ui.login.LoginActivity;
import butterknife.Bind;
import butterknife.OnClick;

public class HomeActivity extends StandardActivity {

    @Bind(R.id.fab)         FloatingActionButton fab;
    @Bind(R.id.tabs)        TabLayout tabLayout;
    @Bind(R.id.container)   ViewPager mViewPager;
    @Bind(R.id.toolbar_toolbar_widget) Toolbar toolbar;
    @Bind(R.id.textview_toolbar_title) TextView toolbarTextView;
    private SectionsPagerAdapter mSectionsPagerAdapter;
    int adapterPosition = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        context = this;
        setupToolbar();

        initListeners();
        setupViewPager();
    }

    private void setupViewPager() {
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(mSectionsPagerAdapter);
        tabLayout.setupWithViewPager(mViewPager);
    }

    private void initListeners() {
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                dateSwipePositionController(position);
            }

            @Override
            public void onPageSelected(int position) {
                adapterPosition = position;
                mSectionsPagerAdapter.onPageChange(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.action_settings:

                return true;
            case R.id.action_logout:
                showPositiveAndNegativeDialog(getString(R.string.sure_logout), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        new SessionData().clearCache();
                        startActivity(new Intent(context, LoginActivity.class));
                        finish();
                    }
                });
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void dateSwipePositionController(int position){
        switch (position){
            case 0:
                if (new DateTime().getDayOfWeek() == 7){
                    fab.hide();
                }else{
                    fab.show();
                    fab.animate().translationY(-(getResources().getDimension(R.dimen.fab_margin_bottom) - getResources().getDimension(R.dimen.fab_margin)))
                            .setDuration(100).setStartDelay(50).start();
                }
                break;
            case 1:
            case 2:
                fab.hide();
                fab.animate().translationY(+(getResources().getDimension(R.dimen.fab_margin_bottom) - getResources().getDimension(R.dimen.fab_margin)))
                        .setDuration(100).setStartDelay(50).start();
                break;
        }
    }

    private void setupToolbar() {

        toolbar.bringToFront();
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);
//        getSupportActionBar().setHomeAsUpIndicator(Drawable.);
        toolbarTextView.setText(getString(R.string.app_name));

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }
    }

    @OnClick(R.id.fab)
    void onFabClick(){
        mSectionsPagerAdapter.onFabClick(adapterPosition);
    }
}
