package br.com.pontoeletronico.ui.home.schedule;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.List;

import br.com.pontoeletronico.R;
import br.com.pontoeletronico.model.Record;


public class ScheduleAdapter extends ArrayAdapter<Record> {

    private ViewHolder holder;
    private LayoutInflater inflater;
    private Context context;
    private ScheduleListener listener;

    public ScheduleAdapter(Context context, List<Record> objects, ScheduleListener listener) {
        super(context, 0, objects);
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.context = context;
        this.listener = listener;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView == null) {
            convertView = inflater.inflate(R.layout.item_main_list, parent, false);

            holder = new ViewHolder();
            holder.content = (TextView) convertView.findViewById(R.id.textview_item_main_time);
            holder.status = (TextView) convertView.findViewById(R.id.textview_item_main_status);

            convertView.setTag(holder);
        }else {
            holder = (ViewHolder) convertView.getTag();
        }

        Record record = getItem(position);

//        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");

        DateTimeFormatter otherFormat = DateTimeFormat.forPattern("HH:mm");
        LocalDateTime localDateTime = new LocalDateTime(record.getDateTime());
        holder.content.setText(otherFormat.print(localDateTime));
        holder.status.setText(position%2==0? "entrada": "saida");

        return convertView;
    }

    private class ViewHolder{

        private TextView content;
        private TextView status;
    }

//    @Override
//    public void remove(Record object) {
//        super.remove(object);
////        APIManager.getInstance().deleteRecord(object);
//        notifyDataSetChanged();
////        DBRecordManager.getInstance().deleteRecordInDatabase(object);
//        listener.onRecordDelete(Record object);
//    }
}
