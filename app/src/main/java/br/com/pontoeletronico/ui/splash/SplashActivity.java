package br.com.pontoeletronico.ui.splash;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import br.com.pontoeletronico.R;
import br.com.pontoeletronico.manager.sharedPreferences.session.SessionData;
import br.com.pontoeletronico.ui.StandardActivity;
import br.com.pontoeletronico.ui.home.HomeActivity;
import br.com.pontoeletronico.ui.login.LoginActivity;
import br.com.pontoeletronico.util.Constants;

/**
 * Created by Chubaca on 10/04/2016.
 */
public class SplashActivity extends StandardActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(new SessionData().getUser() != null && !"".equals(new SessionData().getUser().getUid())){
            //ToDO same as whats done after onAuthenticated()
            //user_login.getAuth().getUid() can be used to getuid, incase
            //you want to pass the uid as a parameter to another activity
            startActivity(new Intent(this, HomeActivity.class));
            finish();
        }else{
            setContentView(R.layout.activity_splash);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent intent = new Intent(context, LoginActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }
            }, Constants.SPLASH_TIME_OUT);
        }
    }
}
