package br.com.pontoeletronico.ui.register;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import br.com.pontoeletronico.R;
import br.com.pontoeletronico.database.WSRecord;
import br.com.pontoeletronico.model.User;
import br.com.pontoeletronico.ui.StandardActivity;
import butterknife.Bind;
import butterknife.OnClick;

public class RegisterActivity extends StandardActivity {

    @Bind(R.id.edittext_register_name)
    EditText nameEditText;
    @Bind(R.id.edittext_register_email)
    EditText emailEditText;
    @Bind(R.id.edittext_register_password)
    EditText passwordEditText;

    private DatabaseReference mDatabase;
    private String mUserName, mUserEmail, mPassword;
    private FirebaseAuth mAuth;
    private String TAG = "Trabalhei!";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);


        mDatabase = FirebaseDatabase.getInstance().getReference();
        mAuth = FirebaseAuth.getInstance();
    }

    /**
     * Create new account using Firebase email/password provider
     */
    @OnClick(R.id.button_register_register)
    public void onCreateAccountPressed(View view) {
        mUserName = nameEditText.getText().toString();
        mUserEmail = emailEditText.getText().toString().toLowerCase();
        mPassword = passwordEditText.getText().toString();

        /**
         * Check that email and user name are okay
         */
        boolean validEmail = isEmailValid(mUserEmail);
        boolean validUserName = isUserNameValid(mUserName);
        boolean validPassword = isPasswordValid(mPassword);
        if (!validEmail || !validUserName || !validPassword) return;

        /**
         * Create new user with specified email and password
         */
        mAuth.createUserWithEmailAndPassword(mUserEmail, mPassword)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {

                        Log.d(TAG, "createUser:onComplete:" + task.isSuccessful());

                        if (task.isSuccessful()) {
                            onAuthSuccess(task.getResult().getUser());
                        } else {
                            Toast.makeText(context, "Sign Up Failed",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    private void onAuthSuccess(FirebaseUser user) {

        String username = mUserName;

        // Write new user
        writeNewUser(user.getUid(), username, user.getEmail());

        showOkAlertDialog("email cadastrado com sucesso!", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                finish();
            }
        });
    }

    // [START basic_write]
    private void writeNewUser(String userId, String name, String email) {
        User user = new User(name, email, userId);
        WSRecord.getInstance().writeUser(null, user);
    }

    private boolean isEmailValid(String email) {
        boolean isGoodEmail =
                (email != null && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches());
        if (!isGoodEmail) {
            Toast.makeText(context, "Email não válido", Toast.LENGTH_SHORT).show();
            return false;
        }
        return isGoodEmail;
    }

    private boolean isUserNameValid(String userName) {
        if (userName.equals("")) {
            Toast.makeText(context, "Nome não pode ser vazio", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private boolean isPasswordValid(String userName) {
        if (userName.toString().equals("")) {
            Toast.makeText(context, "Senha não pode ser vazio", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (userName.length()<6 ) {
            Toast.makeText(context, "Senha não pode ser menor que 6 caracteres", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
