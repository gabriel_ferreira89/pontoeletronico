package br.com.pontoeletronico.ui.home;

import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;
import java.util.List;

import br.com.pontoeletronico.ui.home.monthTab.MonthTabFragment;
import br.com.pontoeletronico.ui.home.schedule.ScheduleFragment;

/**
 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
public class SectionsPagerAdapter extends FragmentStatePagerAdapter {

    private List<StandardFragment> mFragmentList;


    public SectionsPagerAdapter(FragmentManager fm) {
        super(fm);
        mFragmentList = new ArrayList<>();;
    }

    @Override
    public Fragment getItem(int position) {
        // getItem is called to instantiate the fragment for the given page.
        // Return a PlaceholderFragment (defined as a static inner class below).
//        if (position == 2){
        switch (position){
            case 0:
                StandardFragment scheduleFragment = ScheduleFragment.newInstance();
                mFragmentList.add(0, scheduleFragment);
                return scheduleFragment;
            case 1:
                StandardFragment monthTabFragment = MonthTabFragment.newInstance();
                mFragmentList.add(1, monthTabFragment);
                return monthTabFragment;
            case 2:
                return new Fragment();
        }
        return null;
    }

    @Override
    public int getCount() {
        // Show 3 total pages.
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "HOJE";
            case 1:
                return "HISTÓRICO";
            case 2:
                return "ESTATISTICAS";
        }
        return null;
    }

    @Override
    public void restoreState(Parcelable arg0, ClassLoader arg1) {
        //do nothing here! no call to super.restoreState(arg0, arg1);
    }

    public void onFabClick(int position) {
        switch (position){
            case 0:
                mFragmentList.get(0).onFabClick();
                break;
            case 1:
                mFragmentList.get(1).onFabClick();
                break;
            case 2:
                mFragmentList.get(2).onFabClick();
                break;
        }
    }

    public void onPageChange(int position){
        switch (position){
            case 0:
                ((MonthTabFragment)mFragmentList.get(1)).closeExpandableViews();
                break;
            case 1:
                break;
            case 2:
                ((MonthTabFragment)mFragmentList.get(1)).closeExpandableViews();
                break;
        }
    }
}