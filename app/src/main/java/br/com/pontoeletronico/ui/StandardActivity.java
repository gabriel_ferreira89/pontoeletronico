package br.com.pontoeletronico.ui;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

import br.com.pontoeletronico.R;
import br.com.pontoeletronico.manager.sharedPreferences.session.SessionData;
import butterknife.ButterKnife;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


public class StandardActivity extends AppCompatActivity {

    protected Context context;
    private AlertDialog alertDialog;
    private ProgressDialog mConnectionProgressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        context = this;
    }

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        super.setContentView(layoutResID);
        ButterKnife.bind(this);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    protected ProgressDialog getProgressDialog() {
        if(mConnectionProgressDialog == null) {
            mConnectionProgressDialog = new ProgressDialog(this, R.style.AlertDialogStyle);
        }
        return mConnectionProgressDialog;
    }

    public void showProgressDialog(String message) {

        if (!isDestroyed() && !isFinishing()){
            getProgressDialog().setMessage(message);
            getProgressDialog().setCancelable(false);
            getProgressDialog().setCanceledOnTouchOutside(false);
            getProgressDialog().show();
        }
    }
    public void showProgressDialog(String message, boolean bool) {

        getProgressDialog().setMessage(message);
        getProgressDialog().setCancelable(bool);
        getProgressDialog().setCanceledOnTouchOutside(bool);
        getProgressDialog().show();
    }

    public void dismissProgressDialog() {
        getProgressDialog().dismiss();
    }

    public void showOkAlertDialog(String msg, DialogInterface.OnClickListener onClickListener) {
        showOkAlertDialog(msg, onClickListener, false);
    }
    public void showOkAlertDialog(String msg, DialogInterface.OnClickListener onClickListener, boolean cancel) {
        if (context == null || "".equals(msg)){
            return;
        }
        if(alertDialog != null) {
            alertDialog.dismiss();
            alertDialog = null;
        }
        try{
            alertDialog = new android.support.v7.app.AlertDialog.Builder(this, R.style.AlertDialogStyle)
                    .setMessage(msg)
                    .setPositiveButton("OK", onClickListener)
                    .setCancelable(cancel)
                    .show();
        } catch (Exception ignore){

        }
    }

    public void showPositiveAndNegativeDialog(String msg, DialogInterface.OnClickListener onClickListener) {
        if (context == null || "".equals(msg)){
            return;
        }
        if(alertDialog != null) {
            alertDialog.dismiss();
            alertDialog = null;
        }
        alertDialog = new android.support.v7.app.AlertDialog.Builder(this, R.style.AlertDialogStyle)
                .setMessage(msg)
                .setPositiveButton(getResources().getString(R.string.yes), onClickListener)
                .setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //TODO não esta sendo usado.
                    }
                }).show();
        alertDialog.getButton(android.support.v7.app.AlertDialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.white));
    }

    protected void showPositiveAndNegativeDialog(String msg, String positiveMsg, String negativeMsg, DialogInterface.OnClickListener onClickListener) {
        showPositiveAndNegativeDialog(msg, positiveMsg, negativeMsg, onClickListener, null);
    }

    public void showPositiveAndNegativeDialog(String msg, String positiveMsg, String negativeMsg,
                                              DialogInterface.OnClickListener onClickListener,
                                              DialogInterface.OnClickListener negativeOnClickListener) {
        if (context == null || "".equals(msg)){
            return;
        }
        if(alertDialog != null) {
            alertDialog.dismiss();
            alertDialog = null;
        }
        alertDialog = new android.support.v7.app.AlertDialog.Builder(this, R.style.AlertDialogStyle)
                .setMessage(msg)
                .setPositiveButton(positiveMsg, onClickListener)
                .setNegativeButton(negativeMsg, negativeOnClickListener)
                .show();
        if (alertDialog != null) {
            alertDialog.getButton(android.support.v7.app.AlertDialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.white));
        }
    }

    protected void showOkAlertDialog(String msg){
        showOkAlertDialog(msg, null);
    }

    public String getUid() {
        return new SessionData().getUser().getUid();
    }
}
