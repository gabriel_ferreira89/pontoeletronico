package br.com.pontoeletronico.ui.login;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;

import com.facebook.FacebookSdk;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.GoogleAuthProvider;

import br.com.pontoeletronico.R;
import br.com.pontoeletronico.database.WSRecord;
import br.com.pontoeletronico.manager.sharedPreferences.session.SessionData;
import br.com.pontoeletronico.model.User;
import br.com.pontoeletronico.ui.StandardActivity;
import br.com.pontoeletronico.ui.home.HomeActivity;
import br.com.pontoeletronico.ui.register.RegisterActivity;
import br.com.pontoeletronico.util.Constants;
import butterknife.Bind;
import butterknife.OnClick;

public class LoginActivity extends StandardActivity implements GoogleApiClient.OnConnectionFailedListener{

    @Bind(R.id.edittext_login_email)
    EditText emailTextView;
    @Bind(R.id.edittext_login_password)
    EditText passwordTextView;
    private FirebaseAuth mAuth;
    private String TAG = "Trabalhei!";
    private String mUserEmail, mPassword;
    private GoogleSignInOptions gso;
    private GoogleApiClient mGoogleApiClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        FacebookSdk.sdkInitialize(this);
        mAuth = FirebaseAuth.getInstance();
        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
    }

    @OnClick(R.id.textview_login_register)
    void goToRegisterActivity(){
        startActivity(new Intent(this, RegisterActivity.class));
        finish();
    }

    @OnClick(R.id.button_login_login)
    void onLoginClick(){

        mUserEmail = emailTextView.getText().toString().toLowerCase();
        mPassword = passwordTextView.getText().toString();

        /**
         * Check that email and user name are okay
         */
        boolean validEmail = isEmailValid(mUserEmail);
        boolean validPassword = isPasswordValid(mPassword);
        if (!validEmail || !validPassword) return;

        showProgressDialog("Um momento...");
        mAuth.signInWithEmailAndPassword(mUserEmail, mPassword)
                .addOnSuccessListener(this, new OnSuccessListener<AuthResult>() {
                    @Override
                    public void onSuccess(AuthResult authResult) {
                        dismissProgressDialog();
                        showOkAlertDialog("usuario logado com sucesso", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                startActivity(new Intent(context, HomeActivity.class));
                                finish();
                            }
                        });
                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        dismissProgressDialog();
                        Toast.makeText(context, e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void firebaseAuthWithGoogle(final GoogleSignInAccount acct) {
        Log.d(TAG, "firebaseAuthWithGoogle:" + acct.getId());

        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnSuccessListener(this, new OnSuccessListener<AuthResult>() {
                    @Override
                    public void onSuccess(AuthResult authResult) {
                        dismissProgressDialog();
                        writeNewUser(authResult.getUser().getUid(), acct.getDisplayName(), acct.getEmail());
                        showOkAlertDialog("usuario logado com sucesso", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                startActivity(new Intent(context, HomeActivity.class));
                                finish();
                            }
                        });
                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        dismissProgressDialog();
                        Toast.makeText(context, e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private boolean isEmailValid(String email) {
        boolean isGoodEmail =
                (email != null && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches());
        if (!isGoodEmail) {
            Toast.makeText(context, "Email não válido", Toast.LENGTH_SHORT).show();
            return false;
        }
        return isGoodEmail;
    }

    private boolean isPasswordValid(String userName) {
        if (userName.toString().equals("")) {
            Toast.makeText(context, "Nome não pode ser vazio", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    @OnClick(R.id.button_login_google)
    void signInGoogle() {
        showProgressDialog("Um momento...");
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, Constants.RC_SIGN_IN);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == Constants.RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if (result.isSuccess()) {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = result.getSignInAccount();
                firebaseAuthWithGoogle(account);
            } else {
                dismissProgressDialog();
                // Google Sign In failed, update UI appropriately
                // ...
            }
        }
    }

    private void writeNewUser(String userId, String name, String email) {
        final User user = new User(name, email, userId);
        WSRecord.getInstance().writeUser(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                new SessionData().setUser(user);
            }
        }, user);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
