package br.com.pontoeletronico.ui.home;

import android.content.DialogInterface;
import android.support.v4.app.Fragment;

import br.com.pontoeletronico.ui.StandardActivity;

/**
 * Created by Chubaca on 10/04/2016.
 */
public abstract class StandardFragment extends Fragment {

    public abstract void onFabClick();

    protected void showProgressDialog(String message, boolean cancelable) {
        if(getActivity() != null) {
            ((StandardActivity) getActivity()).showProgressDialog(message, cancelable);
        }
    }

    protected void showProgressDialog(String message) {
        if(getActivity() != null) {
            ((StandardActivity)getActivity()).showProgressDialog(message, false);
        }
    }

    protected void dismissProgressDialog(){
        if(getActivity() != null) {
            ((StandardActivity)getActivity()).dismissProgressDialog();
        }
    }

    public void showPositiveAndNegativeDialog(String msg, DialogInterface.OnClickListener onClickListener) {
        if(getActivity() != null) {
            ((StandardActivity)getActivity()).showPositiveAndNegativeDialog(msg, onClickListener);
        }
    }
    public void showPositiveAndNegativeDialog(String msg, String positiveMsg, String negativeMsg,
                                              DialogInterface.OnClickListener onClickListener,
                                              DialogInterface.OnClickListener negativeOnClickListener) {
        if(getActivity() != null) {
            ((StandardActivity)getActivity()).showPositiveAndNegativeDialog(msg, positiveMsg, negativeMsg,
                    onClickListener, negativeOnClickListener);
        }
    }

    protected void showOkAlertDialog(String msg, DialogInterface.OnClickListener onClickListener, boolean cancel){
        if(getActivity() != null) {
            ((StandardActivity)getActivity()).showOkAlertDialog(msg, onClickListener, cancel);
        }
    }

    protected void showOkAlertDialog(String msg, DialogInterface.OnClickListener onClickListener){
        if(getActivity() != null) {
            ((StandardActivity)getActivity()).showOkAlertDialog(msg, onClickListener);
        }
    }

    protected void showOkAlertDialog(String msg){
        if(getActivity() != null) {
            ((StandardActivity)getActivity()).showOkAlertDialog(msg, null);
        }
    }
}
