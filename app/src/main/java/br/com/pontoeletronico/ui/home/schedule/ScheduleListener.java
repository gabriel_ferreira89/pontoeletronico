package br.com.pontoeletronico.ui.home.schedule;


import br.com.pontoeletronico.model.Record;

public interface ScheduleListener {

    void onRecordDelete(Record record);
}
